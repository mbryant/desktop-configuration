#!/usr/bin/env python3
"""
Use `passage` to fetch the ansible vault's key
"""

import subprocess

subprocess.run(["passage", "ansible-key"])
