- hosts: localhost
  connection: local
  become: true
  vars:
    big_drive: dcbaae41-ec9e-4bfd-a87b-eabd19ead4b1
    boot_drive: /dev/nvme0n1p2
  tasks:
  - name: Load secrets
    include_vars: secrets.yml

  - name: Configure tmpfs
    mount:
      path: /tmp
      src: tmpfs
      fstype: tmpfs
      opts: defaults,noatime,mode=1777
      state: present

  - name: Setup the hostname for next reboot
    copy:
      src=etc/hostname
      dest=/etc/hostname

  - name: Install CLI packages needed by root
    community.general.zypper:
      name: ['zsh', 'ripgrep', 'fd']
      state: present

  - import_tasks: tasks/shells.yml
  - import_tasks: tasks/vim.yml

  - name: Setup Nvidia repo
    community.general.zypper_repository:
      name: Nvidia
      repo: 'https://download.nvidia.com/opensuse/tumbleweed'
      state: present
      auto_import_keys: yes
  - name: Install nvidia drivers for GTX 3080
    community.general.zypper:
      name: ['x11-video-nvidiaG06', 'nvidia-glG06', 'nvidia-utils-G06']
      state: present
  - name: Remove opensource nvidia drivers
    community.general.zypper:
      name: ['xf86-video-nouveau']
      state: absent

  - name: Install service packages
    community.general.zypper:
      name: ['nginx', 'openssh', 'nfs-kernel-server', 'certbot', 'python310-certbot-dns-cloudflare']
      state: latest

  - name: Configure SSH
    copy:
      src=etc/ssh/
      dest=/etc/ssh/
    notify: restart sshd

  - name: Mount music drive
    mount:
      path: /mnt/8tb
      src: UUID={{big_drive}}
      fstype: btrfs
      opts: async,rw,noexec,noatime,acl,nofail
      state: mounted

  - name: Copy certbot directory
    copy:
      src=etc/letsencrypt/
      dest=/etc/letsencrypt/
    notify: install certbot

  - name: Generate nginx dhparam
    openssl_dhparam:
      path: /etc/nginx/dhparam.pem
      size: 4096
  - name: Load nginx directory
    copy:
      src: etc/nginx/
      dest: /etc/nginx/
    notify: restart nginx

  - name: Load Cloudflare credentials
    template:
      src: etc/letsencrypt/cloudflare-creds.ini
      dest: /etc/letsencrypt/cloudflare-creds.ini
      mode: "600"
  - name: Rotate letsencrypt certificates as a cron job
    cron:
      minute: '37'
      hour: '13'
      weekday: '4'
      name: "Certbot auto-rotate"
      cron_file: /etc/cron.d/ansible
      user: root
      job: "/usr/bin/certbot renew --dns-cloudflare --dns-cloudflare-credentials /etc/letsencrypt/cloudflare-creds.ini --dns-cloudflare-propagation-seconds 70 --post-hook 'systemctl restart nginx'"

  - name: Setup nfs exports file
    copy:
      src: etc/exports
      dest: /etc/exports
    notify: restart nfsserver

  - name: Add media repo
    community.general.zypper_repository:
      name: Packman
      repo: 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/'
      state: present
      auto_import_keys: yes

  - name: Install multimedia codecs
    community.general.zypper:
      name: ['lame', 'gstreamer-plugins-bad', 'gstreamer-plugins-ugly', 'gstreamer-plugins-ugly-orig-addon', 'gstreamer-plugins-libav', 'vlc-codecs']
      force: yes
      allow_vendor_change: true
      disable_recommends: false
      force_resolution: true
      state: latest

  - name: Install userland packages that cause Nix problems
    community.general.zypper:
      name: ['steam', 'moc', 'yakuake']
      state: latest
      force: yes

  # TODO: Do this at some point
  # - import_tasks: tasks/disk-encryption.yml

  # - name: Setup firewall
  #   import_tasks: tasks/firewall.yml

  handlers:
  - name: restart sshd
    service:
      name: sshd
      state: restarted
      enabled: yes
  - name: restart firewalld
    service:
      name: firewalld
      state: restarted
      enabled: yes
  - name: restart nginx
    service:
      name: nginx
      state: restarted
      enabled: yes
  - name: restart nfsserver
    service:
      name: nfsserver
      state: restarted
      enabled: yes
  - name: install certbot
    debug:
      msg: "Manually generate certs (certbot certonly)"
