# See `man sshd_config` for more information.

# We use port (1337 ^ 0xbeef) to reduce a bit of noise from scanners on port 22
Port 48086

# No one else should be able to SSH in
AllowUsers mbryant
PermitRootLogin no

# Use PAM for passwordbase auth
PasswordAuthentication no
ChallengeResponseAuthentication yes
UsePAM yes

# Crypto options based on https://ssh-audit.com as of Sept 2022
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256
MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com

# Keep sessions from randomly dying
ClientAliveInterval 30
ClientAliveCountMax 3

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile	.ssh/authorized_keys

# This shouldn't be needed, but we leave it around just in case
X11Forwarding yes

# Show our login banner/etc
PrintMotd yes
Banner	/etc/ssh/sshd_banner

# override default of no subsystems
Subsystem	sftp	/usr/lib/ssh/sftp-server

# This enables accepting locale enviroment variables LC_* LANG, see sshd_config(5).
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL

# Reduce the number of weird DNS requests sshd will generate
UseDNS no
