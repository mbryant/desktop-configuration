# Desktop Configuration

### Setup

```
> sudo ansible-galaxy install -r requirements.yml
> echo 127.0.0.1 | sudo tee /etc/ansible/hosts
```

Add `interpreter_python = /usr/bin/python3` under `[defaults]` in `/etc/ansible/ansible.cfg`.


### Running the playbook
sudo -E --preserve-env=PATH ansible-pull -U https://gitlab.com/mbryant/desktop-configuration.git --vault-id=passage-client.py


### From a checkout
sudo -E --preserve-env=PATH ansible-playbook local.yml --vault-id=passage-client.py
